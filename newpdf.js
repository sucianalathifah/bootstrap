var file = 'preview.zip.pdf';

// The workerSrc property shall be specified.
PDFJS.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

var pdfDoc = null,
    pageTotal = 0,
    pageProgress = 0,
    pageRendering = false,
    pageNumPending = null,
    scale = 1,
    canvas = $('#thecanvas')[0],
    ctx = canvas.getContext('2d');

if (!localStorage.getItem('lastPage')) {
    var pageNum = 1
}
else {
    pageNum = parseInt(localStorage.getItem('lastPage'))
}
;
// if (lastPage > 1) {
//     renderPage(lastPage)
// }
/**
 * Get page info from document, resize canvas accordingly, and render page.
 * @param num Page number.
 */
function init() {
    var x = null;
    var y;
    canvas.onmousedown = function (e) {
        x = e.pageX - canvas.offsetLeft;
        y = e.pageY - canvas.offsetTop;
        ctx.beginPath();
        ctx.moveTo(x, y);
    }
    canvas.onmouseup = function (e) {
        x = null;
    }
    canvas.onmousemove = function (e) {
        if (x == null) return;
        x = e.pageX - canvas.offsetLeft;
        y = e.pageY - canvas.offsetLeft;
        ctx.lineTo(x, y);
        ctx.stroke();
    }
}

function renderPage(num) {
    $('#loader').show();
    // $('#thecanvas').hide();

    pageRendering = true;
    // Using promise to fetch the page
    pdfDoc.getPage(num).then(function (page) {
        var viewport = page.getViewport(scale);
        canvas.height = viewport.height;
        canvas.width = viewport.width;

        // Render PDF page into canvas context
        var renderContext = {
            canvasContext: ctx,
            viewport: viewport
        };
        var renderTask = page.render(renderContext);

        // Wait for rendering to finish
        renderTask.promise.then(function () {
            pageRendering = false;
            if (pageNumPending !== null) {
                // New page rendering is pending
                renderPage(pageNumPending);
                pageNumPending = null;
            }

            $('#loader').hide();
            // $('#thecanvas').show();
        });
    });

    // Update page counters

    $('#page_num').text(pageNum);
}

function scaleMin() {
    scale = scale - 0.1;
    scale = scale < 1 ? 1 : scale;
    renderPage(pageNum);
}
function scalePlus() {
    scale = scale + 0.1;
    scale = scale > 3.9 ? 4 : scale;
    renderPage(pageNum);
}
function scale100() {
    scale = 1;
    renderPage(pageNum);
}
/**
 * If another page rendering in progress, waits until the rendering is
 * finised. Otherwise, executes rendering immediately.
 */
function queueRenderPage(num) {
    if (pageRendering) {
        pageNumPending = num;
    } else {
        renderPage(num);
    }
}

/**
 * Displays previous page.
 */
function onPrevPage(e) {
    if (pageNum <= 1) {
        return;
    }
    pageNum--;
    updatePageProgess(pageNum);
    queueRenderPage(pageNum);

    var user = null;

    //getTime
    String.prototype.pad = function (_char, len, to) {
        if (!this || !_char || this.length >= len) {
            return this;
        }
        to = to || 0;

        var ret = this;

        var max = (len - this.length) / _char.length + 1;
        while (--max) {
            ret = (to) ? ret + _char : _char + ret;
        }

        return ret;
    };
    var currentdate = new Date();
    var datetime = currentdate.getFullYear() + "/" + (currentdate.getMonth() + 1 + '').pad('0', 2)
        + "/" + (currentdate.getDate() + '').pad('0', 2) + " "
        + (currentdate.getHours() + '').pad('0', 2) + ":"
        + (currentdate.getMinutes() + '').pad('0', 2) + ":" + (currentdate.getSeconds() + '').pad('0', 2);

    localStorage.setItem('lastPage', pageNum);
    console.log(
        {
            user: user,
            file: file,
            time: datetime,
            page: pageNum
        }
    );
}
$('#prev,.box__nav--left').on('click', onPrevPage);

/**
 * Displays next page.
 */
function onNextPage(e) {
    if (pageNum >= pdfDoc.numPages) {
        return;
    }
    pageNum++;
    updatePageProgess(pageNum);
    queueRenderPage(pageNum);

    var user = null;

    String.prototype.pad = function (_char, len, to) {
        if (!this || !_char || this.length >= len) {
            return this;
        }
        to = to || 0;

        var ret = this;

        var max = (len - this.length) / _char.length + 1;
        while (--max) {
            ret = (to) ? ret + _char : _char + ret;
        }

        return ret;
    };
    var currentdate = new Date();
    var datetime = currentdate.getFullYear() + "/" + (currentdate.getMonth() + 1 + '').pad('0', 2)
        + "/" + (currentdate.getDate() + '').pad('0', 2) + " "
        + (currentdate.getHours() + '').pad('0', 2) + ":"
        + (currentdate.getMinutes() + '').pad('0', 2) + ":" + (currentdate.getSeconds() + '').pad('0', 2);

    localStorage.setItem('lastPage', pageNum);
    console.log(
        {
            user: user,
            file: file,
            time: datetime,
            page: pageNum
        }
    );

}
$('#next,.box__nav--right').on('click', onNextPage);


function updatePageProgess(pageNum) {
    pageProgress = pageNum / pageTotal * 100;
    updateProgess(pageProgress)
}
function updateProgess() {
    var el = document.querySelector(".box__progress-bar");
    var el2 = document.querySelector(".box__progress-pt");
    el.style.width = pageProgress + '%';
    el2.style.left = pageProgress + '%';
}

/**
 * Asynchronously downloads PDF.
 */
PDFJS.getDocument(file).then(function (pdfDoc_) {
    pdfDoc = pdfDoc_;
    pageTotal = pdfDoc.numPages;
    $('#page_count').text(pageTotal);
    renderPage(pageNum);
});

function full_screen() {
    var element = $('html')[0];
    if (!document.fullscreenElement &&    // alternative standard method
        !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement) {  // current working methods
        if (element.requestFullscreen) {
            element.requestFullscreen();
        } else if (element.msRequestFullscreen) {
            element.msRequestFullscreen();
        } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if (element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    } else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
    }

}


function onMouse(element, event) {
    pageProgress = Math.round((event.pageX / element.width()) * 100);
    pageNum = Math.round(pageProgress / 100 * pageTotal);

    queueRenderPage(pageNum);
    updateProgess();
}

$(function () {
    // box progress point
    // var boxProgress = $('.box__progress');
    // boxProgress
    //     .on('click', function (event) {
    //         onMouse(boxProgress, event)
    //     });
    // var isDragging = false;

    // $('.box__progress-pt')
    //     .on('mousedown', function (event) {
    //         console.log('mousedown', event.pageX);
    //         isDragging = true;
    //         // onMouse(boxProgress, event)
    //     });

    // $(window)
    //     .on('mousemove', function (event) {
    //         if (isDragging) {
    //             console.log('mousemove', event.pageX);

    //             pageProgress = Math.round((event.pageX / boxProgress.width()) * 100);
    //             pageNum = Math.round(pageProgress / 100 * pageTotal);

    //             updateProgess();
    //             // _.debounce(function(){queueRenderPage(pageNum)}, 200);
    //         }
    //     })
    //     .on('mouseup', function (event) {
    //         if (isDragging) {
    //             isDragging = false;
    //             console.log('mouseup', event.pageX);
    //             onMouse(boxProgress, event)
    //         }
    //     });

    // disable canvas right click
    $("#thecanvas").on("contextmenu", function () {
        return false;
    });
    // select all on focus
    $("input:text").focus(function () {
        $(this).select();
    });
    // right and left keyboard
    $(document).keydown(function (event) {
        if (event.keyCode == 39) {
            $('#next').click(); //on left arrow, click next (since your next is on the left)
        } else if (event.keyCode == 37) {
            $('#prev').click(); //on right arrow, click prev
        }
    });

    // change page by textbox
    $('#page_num').keyup(function (e) {

        if (e.keyCode == 13) {
            pageNum = parseInt($(this).val());
            queueRenderPage(parseInt($(this).val()));  //on enter arrow, click enter
        }
    });

    // show / hide control bar
    var timedelay = 1;
    var _delay = setInterval(delayCheck, 500);

    $(window).on('mousemove', function (e) {
        $('.box__control').fadeIn();
        timedelay = 1;
        clearInterval(_delay);
        if ($(e.target).closest('.box__control').length < 1) {
            _delay = setInterval(delayCheck, 500);
        }
    });

    function delayCheck() {
        if (timedelay == 3) {
            $('.box__control').fadeOut();
            timedelay = 1;
        }
        timedelay = timedelay + 1;
    }
});





